package t1.dkhrunina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.api.endpoint.ISystemEndpoint;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.request.system.ServerAboutRequest;
import t1.dkhrunina.tm.dto.request.system.ServerVersionRequest;
import t1.dkhrunina.tm.dto.response.system.ServerAboutResponse;
import t1.dkhrunina.tm.dto.response.system.ServerVersionResponse;
import t1.dkhrunina.tm.marker.SoapCategory;
import t1.dkhrunina.tm.service.PropertyService;

@Category(SoapCategory.class)
public class SystemEndpointTest {

    @NotNull
    private static ISystemEndpoint systemEndpoint;

    @Before
    public void initEndpoint() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        systemEndpoint = ISystemEndpoint.newInstance(propertyService);
    }

    @Test
    public void aboutTest() {
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        @Nullable final ServerAboutResponse response = systemEndpoint.getAbout(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getName());
        Assert.assertNotNull(response.getApplicationName());
        Assert.assertNotNull(response.getEmail());
        Assert.assertNotNull(response.getGitBranch());
        Assert.assertNotNull(response.getGitCommitId());
        Assert.assertNotNull(response.getGitCommitMessage());
        Assert.assertNotNull(response.getGitCommitTime());
        Assert.assertNotNull(response.getGitCommitter());
        Assert.assertNotNull(response.getGitCommitterEmail());
    }

    @Test
    public void versionTest() {
        @NotNull final ServerVersionRequest request = new ServerVersionRequest();
        @Nullable final ServerVersionResponse response = systemEndpoint.getVersion(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getVersion());
    }

}
