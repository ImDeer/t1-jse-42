package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.api.repository.ICommandRepository;
import t1.dkhrunina.tm.api.service.ICommandService;
import t1.dkhrunina.tm.command.AbstractCommand;
import t1.dkhrunina.tm.command.system.ApplicationAboutCommand;
import t1.dkhrunina.tm.command.system.ApplicationVersionCommand;
import t1.dkhrunina.tm.marker.UnitCategory;
import t1.dkhrunina.tm.repository.CommandRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitCategory.class)
public class CommandServiceTest {

    @NotNull
    private ICommandService commandService;

    @NotNull
    private AbstractCommand testCommand;

    @Before
    public void initRepository() {
        @NotNull final ICommandRepository commandRepository = new CommandRepository();
        commandService = new CommandService(commandRepository);
        testCommand = new ApplicationAboutCommand();
        commandService.add(testCommand);
    }

    @Test
    public void testAdd() {
        @NotNull final AbstractCommand command = new ApplicationVersionCommand();
        commandService.add(command);
        @Nullable final AbstractCommand newCommand = commandService.getCommandByName(command.getName());
        Assert.assertNotNull(newCommand);
        Assert.assertEquals(command, newCommand);
    }

    @Test
    public void testAddNull() {
        int numberOfTerminalCommands = commandService.getTerminalCommands().size();
        commandService.add(null);
        Assert.assertEquals(numberOfTerminalCommands, commandService.getTerminalCommands().size());
    }

    @Test
    public void testGetCommandByArgument() {
        @Nullable final String argument = testCommand.getArgument();
        Assert.assertNotNull(argument);
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByArgument(argument);
        Assert.assertNotNull(commandFromRepository);
        Assert.assertEquals(testCommand, commandFromRepository);
    }

    @Test
    public void testGetCommandByArgumentNull() {
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByArgument(null);
        Assert.assertNull(commandFromRepository);
    }

    @Test
    public void testGetCommandByArgumentEmpty() {
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByArgument("");
        Assert.assertNull(commandFromRepository);
    }

    @Test
    public void testGetCommandByName() {
        @Nullable final String name = testCommand.getName();
        Assert.assertNotNull(name);
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByName(name);
        Assert.assertNotNull(commandFromRepository);
        Assert.assertEquals(testCommand, commandFromRepository);
    }

    @Test
    public void testGetCommandByNameNull() {
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByName(null);
        Assert.assertNull(commandFromRepository);
    }

    @Test
    public void testGetCommandByNameEmpty() {
        @Nullable final AbstractCommand commandFromRepository = commandService.getCommandByName("");
        Assert.assertNull(commandFromRepository);
    }

    @Test
    public void testGetCommandsWithArgument() {
        @NotNull final Iterable<AbstractCommand> commands = commandService.getCommandsWithArgument();
        @NotNull final List<AbstractCommand> commandList = new ArrayList<>();
        for(AbstractCommand command : commands) commandList.add(command);
        Assert.assertTrue(commandList.size() > 0);
    }

    @Test
    public void testGetTerminalCommands() {
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        Assert.assertTrue(commands.size() > 0);
    }

}