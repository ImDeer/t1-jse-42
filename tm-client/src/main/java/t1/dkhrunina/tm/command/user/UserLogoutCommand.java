package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.user.UserLogoutRequest;
import t1.dkhrunina.tm.dto.response.user.UserLogoutResponse;
import t1.dkhrunina.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "u-logout";

    @NotNull
    private static final String DESCRIPTION = "Logout current user.";

    @Override
    public void execute() {
        System.out.println("[User logout]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        @NotNull final UserLogoutResponse response = getAuthEndpoint().logoutUser(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}