package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataSaveJsonFasterXmlRequest;

public final class DataSaveJsonFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-s-json-faster";

    @NotNull
    private static final String DESCRIPTION = "Save data to FasterXML JSON file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().saveJsonFasterXmlData(new DataSaveJsonFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}