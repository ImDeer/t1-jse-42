package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.task.TaskClearRequest;
import t1.dkhrunina.tm.dto.response.task.TaskClearResponse;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-clear";

    @NotNull
    private static final String DESCRIPTION = "Delete all tasks.";

    @Override
    public void execute() {
        System.out.println("[Clear task list]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        @NotNull final TaskClearResponse response = getTaskEndpoint().clearTask(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}