package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.project.ProjectUpdateByIndexRequest;
import t1.dkhrunina.tm.dto.response.project.ProjectUpdateByIndexResponse;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-update-by-index";

    @NotNull
    private static final String DESCRIPTION = "Update project by index.";

    @Override
    public void execute() {
        System.out.println("[Update project by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description (optional): ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(getToken(), index, name, description);
        @NotNull final ProjectUpdateByIndexResponse response = getProjectEndpoint().updateProjectByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}