package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.user.UserUnlockRequest;
import t1.dkhrunina.tm.dto.response.user.UserUnlockResponse;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "u-unlock";

    @NotNull
    private static final String DESCRIPTION = "Unlock user by login .";

    @Override
    public void execute() {
        System.out.println("[Unlock user]");
        System.out.println("Enter login: ");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken(), login);
        @NotNull final UserUnlockResponse response = getUserEndpoint().unlockUser(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}