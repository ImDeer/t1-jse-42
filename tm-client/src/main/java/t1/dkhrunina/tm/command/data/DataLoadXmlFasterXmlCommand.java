package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataLoadXmlFasterXmlRequest;

public final class DataLoadXmlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-l-xml-faster";

    @NotNull
    private static final String DESCRIPTION = "Load data from FasterXML XML file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().loadXmlFasterXmlData(new DataLoadXmlFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}