package t1.dkhrunina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.constant.DBConst;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModel extends AbstractModel {

    private static final long serialVersionUID = 1;

    @Nullable
    @ManyToOne
    @JoinColumn(name = DBConst.COLUMN_USER_ID)
    private User user;

}