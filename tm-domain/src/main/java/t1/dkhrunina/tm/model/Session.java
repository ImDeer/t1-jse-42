package t1.dkhrunina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.constant.DBConst;
import t1.dkhrunina.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConst.TABLE_SESSION)
public final class Session extends AbstractUserOwnedModel {

    @Nullable
    @Enumerated(EnumType.STRING)
    @Column(name = DBConst.COLUMN_ROLE, length = 50)
    private Role role = null;

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(@NotNull final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Session)) return false;
        @NotNull final Session session = (Session) obj;
        return session.getId().equals(this.getId());
    }

}