package t1.dkhrunina.tm.api.model;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}