package t1.dkhrunina.tm.exception.user;

public class LoginExistsException extends AbstractUserException {

    public LoginExistsException() {
        super("Error: user with this login already exists");
    }

}