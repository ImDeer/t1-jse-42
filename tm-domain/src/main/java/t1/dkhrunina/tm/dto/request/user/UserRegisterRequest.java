package t1.dkhrunina.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractRequest;
import t1.dkhrunina.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public class UserRegisterRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    @Nullable
    private Role role;

    public UserRegisterRequest(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public UserRegisterRequest(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
    }

}