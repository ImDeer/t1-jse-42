package t1.dkhrunina.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskRemoveByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskRemoveByIndexRequest(
            @Nullable final String token,
            @Nullable final Integer index
    ) {
        super(token);
        this.index = index;
    }

}