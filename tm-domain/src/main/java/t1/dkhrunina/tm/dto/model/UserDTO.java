package t1.dkhrunina.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.constant.DBConst;
import t1.dkhrunina.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConst.TABLE_USER)
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = DBConst.COLUMN_LOGIN)
    private String login;

    @Nullable
    @Column(name = DBConst.COLUMN_PASSWORD)
    private String passwordHash;

    @Nullable
    @Column(name = DBConst.COLUMN_EMAIL)
    private String email;

    @Nullable
    @Column(name = DBConst.COLUMN_FIRST_NAME)
    private String firstName;

    @Nullable
    @Column(name = DBConst.COLUMN_LAST_NAME)
    private String lastName;

    @Nullable
    @Column(name = DBConst.COLUMN_MIDDLE_NAME)
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = DBConst.COLUMN_ROLE, length = 50, nullable = false)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = DBConst.COLUMN_LOCKED, nullable = false)
    private Boolean locked = false;

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(@NotNull final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof UserDTO)) return false;
        @NotNull final UserDTO user = (UserDTO) obj;
        return user.getId().equals(this.getId());
    }

}