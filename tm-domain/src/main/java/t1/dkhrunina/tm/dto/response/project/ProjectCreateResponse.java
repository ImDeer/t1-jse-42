package t1.dkhrunina.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCreateResponse extends AbstractResultResponse {

    @Nullable
    private ProjectDTO project;

    public ProjectCreateResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

    public ProjectCreateResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}