package t1.dkhrunina.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.model.IWBS;
import t1.dkhrunina.tm.constant.DBConst;
import t1.dkhrunina.tm.enumerated.Status;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConst.TABLE_TASK)
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = DBConst.COLUMN_NAME, nullable = false)
    private String name = "";

    @Nullable
    @Column(name = DBConst.COLUMN_DESCRIPTION)
    private String description = "";

    @Nullable
    @Column(name = DBConst.COLUMN_PROJECT_ID, length = 36)
    private String projectId;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = DBConst.COLUMN_STATUS, length = 50, nullable = false)
    private Status status = Status.NOT_STARTED;

    public TaskDTO(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    @Override
    public String toString() {
        return name + (description == null || description.isEmpty() ? "" : (": " + description));
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(@NotNull final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof TaskDTO)) return false;
        @NotNull final TaskDTO task = (TaskDTO) obj;
        return task.getId().equals(this.getId());
    }

}