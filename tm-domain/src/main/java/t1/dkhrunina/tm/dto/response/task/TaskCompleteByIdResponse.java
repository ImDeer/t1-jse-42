package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskCompleteByIdResponse extends AbstractResultResponse {

    @NotNull
    private TaskDTO task;

    public TaskCompleteByIdResponse(@NotNull final TaskDTO task) {
        this.task = task;
    }

    public TaskCompleteByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}