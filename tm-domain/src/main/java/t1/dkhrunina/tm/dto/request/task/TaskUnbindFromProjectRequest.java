package t1.dkhrunina.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskUnbindFromProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public TaskUnbindFromProjectRequest(
            @Nullable final String token,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        super(token);
        this.projectId = projectId;
        this.taskId = taskId;
    }

}