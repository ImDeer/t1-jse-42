package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.IdEmptyException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.dto.model.SessionDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SessionServiceTest {

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private UserDTO user;

    @NotNull
    private UserDTO admin;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private IUserService userService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        sessionService = new SessionService(connectionService);
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", "admin@admin.admin", Role.ADMIN);
        @NotNull final SessionDTO session1 = new SessionDTO();
        session1.setUserId(user.getId());
        session1.setRole(user.getRole());
        sessionService.add(user.getId(), session1);
        @NotNull final SessionDTO session2 = new SessionDTO();
        session2.setUserId(admin.getId());
        session2.setRole(admin.getRole());
        sessionService.add(admin.getId(), session2);
        sessionList = new ArrayList<>();
        sessionList.add(session1);
        sessionList.add(session2);
    }

    @After
    public void afterTest() throws Exception {
        sessionService.clear(user.getId());
        sessionService.clear(admin.getId());
        userService.remove(user);
        userService.remove(admin);
    }

    @Test
    public void testAddForUser() throws Exception {
        int expectedNumberOfEntries = sessionService.getSize(user.getId()) + 1;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        sessionService.add(user.getId(), session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testAddForUserNull() throws Exception {
        int expectedNumberOfEntries = sessionService.getSize(user.getId());
        @Nullable final SessionDTO session = sessionService.add(user.getId(), null);
        Assert.assertNull(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testClearForUser() throws Exception {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(sessionService.getSize(user.getId()) > 0);
        sessionService.clear(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testExistById() throws Exception {
        for (@NotNull final SessionDTO session : sessionList) {
            final boolean isExist = sessionService.existsById(session.getUserId(), session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdForUser() throws Exception {
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final SessionDTO session : sessionsForTestUser) {
            final boolean isExist = sessionService.existsById(user.getId(), session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdForUserFalse() throws Exception {
        final boolean isExist = sessionService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testFindAllForUser() throws Exception {
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable final List<SessionDTO> sessions = sessionService.findAll(user.getId());
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionsForTestUser, sessions);
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable SessionDTO foundSession;
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final SessionDTO session : sessionsForTestUser) {
            foundSession = sessionService.findOneById(user.getId(), session.getId());
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void testFindOneByIdForUserNull() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(user.getId(), null));
    }

    @Test
    public void testFindOneByIdForUserEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(user.getId(), ""));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionsForTestUser.size(); i++) {
            @Nullable final SessionDTO session = sessionService.findOneByIndex(user.getId(), i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void testFindOneByIndexIncorrectForUser() {
        int index = sessionService.getSize(user.getId()) + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(user.getId(), index));
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(user.getId(), null));
    }

    @Test
    public void testFindOneByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(user.getId(), -1));
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .count();
        int actualSize = sessionService.getSize(user.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        for (@NotNull final SessionDTO session : sessionList) {
            @NotNull final String sessionId = session.getId();
            @Nullable final SessionDTO deletedSession = sessionService.remove(session);
            Assert.assertNotNull(deletedSession);
            @Nullable final SessionDTO deletedSessionInRepository = sessionService.findOneById(session.getUserId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test
    public void testRemoveEntityNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.remove(null));
    }

    @Test
    public void testRemoveByIdForUser() throws Exception {
        @NotNull final List<SessionDTO> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final SessionDTO session : sessionsForTestUser) {
            @NotNull final String sessionId = session.getId();
            @Nullable final SessionDTO deletedSession = sessionService.removeById(user.getId(), sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final SessionDTO deletedSessionInRepository = sessionService.findOneById(user.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test
    public void testRemoveByIdNullForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(user.getId(), null));
    }

    @Test
    public void testRemoveByIdEmptyForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(user.getId(), ""));
    }

    @Test
    public void testRemoveByIdNotFoundForUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.removeById(user.getId(), "meow"));
    }

    @Test
    public void testRemoveByIndexForUser() throws Exception {
        int index = (int) sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final SessionDTO deletedSession = sessionService.removeByIndex(user.getId(), index);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final SessionDTO deletedSessionInRepository = sessionService.findOneById(user.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexInvalidForUser() {
        int index = sessionList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(user.getId(), index));
    }

    @Test
    public void testRemoveByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveByIndexNegaiveForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(user.getId(), -1));
    }

}