package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.entity.TaskNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskServiceTest {

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private UserDTO user;

    @NotNull
    private UserDTO admin;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private IUserService userService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        userService.clear();
        taskService.clear();
        taskList = new ArrayList<>();
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", "admin@admin.admin", Role.ADMIN);
        @NotNull final ProjectDTO project1 = projectService.create(user.getId(), "project 1", "user project 1");
        @NotNull final ProjectDTO project2 = projectService.create(user.getId(), "project 2", "user project 2");
        @NotNull final ProjectDTO project3 = projectService.create(admin.getId(), "project 3", "admin project");
        @NotNull final TaskDTO task1 = taskService.create(user.getId(), "task 1", "project 1 task");
        task1.setProjectId(project1.getId());
        taskService.update(task1);
        @NotNull final TaskDTO task2 = taskService.create(user.getId(), "task 2", "project 2 task");
        task2.setProjectId(project2.getId());
        taskService.update(task2);
        @NotNull final TaskDTO task3 = taskService.create(admin.getId(), "task 3", "project 3 task");
        task3.setProjectId(project3.getId());
        taskService.update(task3);
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task3);
    }

    @After
    public void afterTest() throws Exception {
        userService.remove(user);
        userService.remove(admin);
    }

    @Test
    public void testAddForUser() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(user.getId()) + 1;
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(user.getId());
        task.setName("Test Name");
        task.setDescription("Test Description");
        taskService.add(user.getId(), task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testAddNullForUser() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(user.getId());
        @Nullable final TaskDTO task = taskService.add(user.getId(), null);
        Assert.assertNull(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testChangeStatusById() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(user.getId());
        Assert.assertNotNull(tasks);
        for (@NotNull final TaskDTO task : tasks) {
            @NotNull final String taskId = task.getId();
            @Nullable TaskDTO changedTask = taskService.changeTaskStatusById(user.getId(), taskId, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(user.getId(), taskId);
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test
    public void testChangeStatusByIdEmpty() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.changeTaskStatusById(user.getId(), "", Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusByIdNull() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.changeTaskStatusById(user.getId(), null, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusByIdNotFound() {
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.changeTaskStatusById(user.getId(), "123", Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusIncorrectById() {
        Assert.assertThrows(StatusIncorrectException.class, () -> taskService.changeTaskStatusById(user.getId(), "meow", null));
    }

    @Test
    public void testChangeStatusByIndex() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(user.getId());
        Assert.assertNotNull(tasks);
        for (int i = 1; i <= tasks.size(); i++) {
            @NotNull final TaskDTO task = tasks.get(i - 1);
            @NotNull final String taskId = task.getId();
            @Nullable TaskDTO changedTask = taskService.changeTaskStatusByIndex(user.getId(), i, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(user.getId(), taskId);
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test
    public void testChangeStatusByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(user.getId(), null, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(user.getId(), -1, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusByIndexInvalid() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(user.getId(), 100, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusIncorrectByIndex() {
        Assert.assertThrows(StatusIncorrectException.class, () -> taskService.changeTaskStatusByIndex(user.getId(), 1, null));
    }

    @Test
    public void testClearForUser() throws Exception {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(taskService.getSize(user.getId()) > 0);
        taskService.clear(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testCreate() {
        int expectedNumberOfEntries = taskService.getSize(user.getId()) + 1;
        @NotNull final String name = "Task name";
        @NotNull final String description = "Task Description";
        @Nullable TaskDTO createdTask = taskService.create(user.getId(), name, description);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(user.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(description, createdTask.getDescription());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testCreateNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), "", "description"));
    }

    @Test
    public void testCreateNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), null, "description"));
    }

    @Test
    public void testCreateDescriptionEmpty() {
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(user.getId(), "name", ""));
    }

    @Test
    public void testCreateDescriptionNull() {
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(user.getId(), "name", null));
    }

    @Test
    public void testCreateByName() {
        int expectedNumberOfEntries = taskService.getSize(user.getId()) + 1;
        @NotNull final String name = "Task name";
        @Nullable TaskDTO createdTask = taskService.create(user.getId(), name);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(user.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testCreateByNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), ""));
    }

    @Test
    public void testCreateByNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), null));
    }

    @Test
    public void testExistByIdForUser() throws Exception {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        for (@NotNull final TaskDTO task : tasksForTestUser) {
            final boolean isExist = taskService.existsById(user.getId(), task.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdForUserFalse() throws Exception {
        final boolean isExist = taskService.existsById("meow", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testFindAllByProjectId() {
        @Nullable final ProjectDTO project = projectService.findOneByIndex(user.getId(), 1);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<TaskDTO> tasksToFind = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).filter(m -> projectId.equals(m.getProjectId())).collect(Collectors.toList());
        @Nullable final List<TaskDTO> tasksByProjectId = taskService.findAllByProjectId(user.getId(), projectId);
        Assert.assertNotNull(tasksByProjectId);
        Assert.assertEquals(tasksToFind.size(), tasksByProjectId.size());
    }

    @Test
    public void testFindAllByProjectIdEmpty() {
        @NotNull final List<TaskDTO> emptyList = Collections.emptyList();
        @Nullable final List<TaskDTO> tasksByProjectId = taskService.findAllByProjectId(user.getId(), "");
        Assert.assertNotNull(tasksByProjectId);
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    public void testFindAllByProjectIdNull() {
        @NotNull final List<TaskDTO> emptyList = Collections.emptyList();
        @Nullable final List<TaskDTO> tasksByProjectId = taskService.findAllByProjectId(user.getId(), null);
        Assert.assertNotNull(tasksByProjectId);
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    public void testFindAll() {
        @Nullable final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
    }

    @Test
    public void testFindAllForUser() throws Exception {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        @Nullable final List<TaskDTO> tasks = taskService.findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUser() {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        @Nullable Comparator<TaskDTO> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<TaskDTO> tasks = taskService.findAll(user.getId(), comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskService.findAll(user.getId(), comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskService.findAll(user.getId(), comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
    }

    @Test
    public void testFindAllWithSortForUser() {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        @Nullable List<TaskDTO> tasks = taskService.findAll(user.getId(), Sort.BY_NAME);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        tasks = taskService.findAll(user.getId(), Sort.BY_CREATED);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        tasks = taskService.findAll(user.getId(), Sort.BY_STATUS);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        @Nullable final Sort sort = null;
        tasks = taskService.findAll(user.getId(), sort);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable TaskDTO foundTask;
        @NotNull final List<TaskDTO> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        for (@NotNull final TaskDTO task : tasksForTestUser) {
            foundTask = taskService.findOneById(user.getId(), task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void testFindOneByIdNullForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(user.getId(), null));
    }

    @Test
    public void testFindOneByIdEmptyForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(user.getId(), ""));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= tasksForTestUser.size(); i++) {
            @Nullable final TaskDTO task = taskService.findOneByIndex(user.getId(), i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void testFindOneByIndexInvalidForUser() {
        int index = taskService.getSize(user.getId()) + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(user.getId(), index));
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(user.getId(), null));
    }

    @Test
    public void testFindOneByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(user.getId(), -1));
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) taskList.stream().filter(m -> user.getId().equals(m.getUserId())).count();
        int actualSize = taskService.getSize(user.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveAllByProjectId() {
        @Nullable final ProjectDTO project = projectService.findOneByIndex(user.getId(), 1);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @Nullable final List<TaskDTO> tasksByProjectId = taskService.findAllByProjectId(user.getId(), projectId);
        Assert.assertNotNull(tasksByProjectId);
        Assert.assertTrue(tasksByProjectId.size() > 0);
        taskService.removeAllByProjectId(user.getId(), projectId);
        @Nullable final List<TaskDTO> tasksAfterDelete = taskService.findAllByProjectId(user.getId(), projectId);
        Assert.assertNotNull(tasksAfterDelete);
        Assert.assertEquals(0, tasksAfterDelete.size());
    }

    @Test
    public void testRemoveByIdForUser() throws Exception {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        for (@NotNull final TaskDTO task : tasksForTestUser) {
            @NotNull final String taskId = task.getId();
            @Nullable final TaskDTO deletedTask = taskService.removeById(user.getId(), taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final TaskDTO deletedTaskInRepository = taskService.findOneById(user.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test
    public void testRemoveByIdNullForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(user.getId(), null));
    }

    @Test
    public void testRemoveByIdEmptyForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(user.getId(), ""));
    }

    @Test
    public void testRemoveByIdNotFoundForUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.removeById(user.getId(), "123321"));
    }

    @Test
    public void testRemoveByIndexForUser() throws Exception {
        int index = (int) taskList.stream().filter(m -> user.getId().equals(m.getUserId())).count();
        while (index > 0) {
            @Nullable final TaskDTO deletedTask = taskService.removeByIndex(user.getId(), index);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final TaskDTO deletedTaskInRepository = taskService.findOneById(user.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexInvalidForUser() {
        int index = taskList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(user.getId(), index));
    }

    @Test
    public void testRemoveByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(user.getId(), -1));
    }

    @Test
    public void testUpdateById() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(user.getId());
        Assert.assertNotNull(tasks);
        @NotNull String name;
        @NotNull String description;
        int index = 0;
        for (@NotNull final TaskDTO task : tasks) {
            @NotNull final String taskId = task.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable TaskDTO updatedTask = taskService.updateById(user.getId(), taskId, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(user.getId(), taskId);
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test
    public void testUpdateByIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(user.getId(), "", "name", "description"));
    }

    @Test
    public void testUpdateByIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(user.getId(), null, "name", "description"));
    }

    @Test
    public void testUpdateByIdNotFound() {
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.updateById(user.getId(), "meow", "name", "description"));
    }

    @Test
    public void testUpdateByIdNameEmpty() {
        @NotNull final String id = taskService.findOneByIndex(user.getId(), 1).getId();
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(user.getId(), id, "", "description"));
    }

    @Test
    public void testUpdateByIdNameNull() {
        @NotNull final String id = taskService.findOneByIndex(user.getId(), 1).getId();
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(user.getId(), id, null, "description"));
    }

    @Test
    public void testUpdateByIndex() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(user.getId());
        Assert.assertNotNull(tasks);
        @NotNull String name;
        @NotNull String description;
        int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            @NotNull final String taskId = task.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable TaskDTO updatedTask = taskService.updateByIndex(user.getId(), index, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(user.getId(), taskId);
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test
    public void testUpdateByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(user.getId(), null, "name", "description"));
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(user.getId(), -1, "name", "description"));
    }

    @Test
    public void testUpdateByIndexInvalid() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(user.getId(), 100, "name", "description"));
    }

    @Test
    public void testUpdateByIndexNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(user.getId(), 1, null, "description"));
    }

    @Test
    public void testUpdateByIndexNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(user.getId(), 1, "", "description"));
    }

}