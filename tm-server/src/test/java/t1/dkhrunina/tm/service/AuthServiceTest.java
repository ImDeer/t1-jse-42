package t1.dkhrunina.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.dto.model.SessionDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.field.LoginEmptyException;
import t1.dkhrunina.tm.exception.field.PasswordEmptyException;
import t1.dkhrunina.tm.exception.user.AccessDeniedException;
import t1.dkhrunina.tm.util.CryptUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AuthServiceTest {

    @NotNull
    private IAuthService authService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private List<UserDTO> userList;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private UserDTO user;

    @NotNull
    private List<SessionDTO> sessionList;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        propertyService = new PropertyService();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        sessionService = new SessionService(connectionService);
        authService = new AuthService(propertyService, userService, sessionService);
        @NotNull final UserDTO test = userService.create("Test", "Test", "test@email.com");
        @NotNull final UserDTO admin = userService.create("Admin", "Admin", "admin@admin.admin", Role.ADMIN);
        user = userService.create("User", "User", "user@user.user", Role.ADMIN);
        userList = new ArrayList<>();
        userList.addAll(userService.findAll());
        @NotNull final SessionDTO session1 = new SessionDTO();
        session1.setUserId(test.getId());
        session1.setRole(test.getRole());
        sessionService.add(test.getId(), session1);
        @NotNull final SessionDTO session2 = new SessionDTO();
        session2.setUserId(admin.getId());
        session2.setRole(admin.getRole());
        sessionService.add(admin.getId(), session2);
        @NotNull final SessionDTO session3 = new SessionDTO();
        session3.setUserId(user.getId());
        session3.setRole(user.getRole());
        sessionService.add(user.getId(), session3);
        sessionList = new ArrayList<>();
        sessionList.add(session1);
        sessionList.add(session2);
        sessionList.add(session3);
    }

    @After
    public void afterTest() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            sessionService.clear(user.getId());
        }
        for (@NotNull final UserDTO user : userList) {
            userService.remove(user);
        }
    }

    @Test
    public void testLogin() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
        }
    }

    @Test
    public void testLoginLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> authService.login("", user.getLogin()));
    }

    @Test
    public void testLoginLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> authService.login(null, user.getLogin()));
    }

    @Test
    public void testLoginUserNotFound() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.login("NewUser", "Pass"));
    }

    @Test
    public void testLoginPassEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> authService.login(user.getLogin(), ""));
    }

    @Test
    public void testLoginPassNull() {
        Assert.assertThrows(PasswordEmptyException.class, () -> authService.login(user.getLogin(), null));
    }

    @Test
    public void testLoginPassWrong() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.login(user.getLogin(), "WrongPass"));
    }

    @Test
    public void testLoginUserLocked() throws Exception {
        userService.lockUserByLogin(user.getLogin());
        Assert.assertThrows(AccessDeniedException.class, () -> authService.login(user.getLogin(), user.getLogin()));
    }

    @Test
    public void testLogout() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
            @Nullable final SessionDTO validSession = authService.validateToken(token);
            Assert.assertNotNull(validSession);
            boolean isSessionExists = sessionService.existsById(validSession.getUserId(), validSession.getId());
            Assert.assertTrue(isSessionExists);
            authService.logout(token);
            isSessionExists = sessionService.existsById(validSession.getUserId(), validSession.getId());
            Assert.assertFalse(isSessionExists);
            authService.logout(token);
        }
    }

    @Test
    public void testLogoutTokenNull() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.logout(null));
    }

    @Test
    public void testLogoutTokenInvalid() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.logout("meow"));
    }

    @Test
    public void testValidateToken() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
            @NotNull final SessionDTO validSession = authService.validateToken(token);
            Assert.assertNotNull(validSession);
            Assert.assertNotNull(validSession.getUserId());
            boolean isSessionExists = sessionService.existsById(validSession.getUserId(), validSession.getId());
            Assert.assertTrue(isSessionExists);
        }
    }

    @Test
    public void testValidateTokenNull() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken(null));
    }

    @Test
    public void testValidateTokenInvalid() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken("meow"));
    }

    @Test
    public void testValidateTokenExpiredSession() throws Exception {
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        @NotNull final SessionDTO validSession = authService.validateToken(token);
        Assert.assertNotNull(validSession);
        @NotNull final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -30);
        @NotNull final Date dateBefore = cal.getTime();
        validSession.setCreated(dateBefore);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String tokenForMapper = objectMapper.writeValueAsString(validSession);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String invalidToken = CryptUtil.encrypt(sessionKey, tokenForMapper);
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken(invalidToken));
    }

    @Test
    public void testValidateTokenSessionNull() throws Exception {
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        @NotNull final SessionDTO validSession = authService.validateToken(token);
        Assert.assertNotNull(validSession);
        @NotNull final String sessionId = validSession.getId();
        @NotNull final String userId = validSession.getUserId();
        Assert.assertNotNull(userId);
        sessionService.removeById(userId, sessionId);
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken(token));
    }

    @Test
    public void testInvalidate() throws Exception {
        @Nullable final List<SessionDTO> sessionList = sessionService.findAll();
        Assert.assertNotNull(sessionList);
        Assert.assertTrue(sessionList.size() > 0);
        for (@NotNull final SessionDTO session : sessionList) {
            authService.invalidate(session);
        }
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void testInvalidateNull() throws Exception {
        int expectedNumberOfEntries = sessionService.findAll().size();
        authService.invalidate(null);
        int actualNumberOfEntries = sessionService.findAll().size();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void testRegister() throws Exception {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASSWORD";
        @NotNull final String email = "EMAIL";
        @NotNull final UserDTO user = authService.register(login, password, email);
        Assert.assertNotNull(user);
        @Nullable final UserDTO newUser = userService.findOneById(user.getId());
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(email, newUser.getEmail());
        @Nullable final String token = authService.login(newUser.getLogin(), "PASSWORD");
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        authService.logout(token);
        userService.remove(newUser);
    }

}