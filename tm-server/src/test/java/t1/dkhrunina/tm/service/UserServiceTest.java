package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.exception.user.EmailExistsException;
import t1.dkhrunina.tm.exception.user.LoginExistsException;
import t1.dkhrunina.tm.exception.user.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private List<UserDTO> userList;

    @NotNull
    private IUserService userService;

    @NotNull
    private IAuthService authService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private UserDTO admin;

    @NotNull
    private UserDTO user;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        @NotNull final ISessionService sessionService = new SessionService(connectionService);
        authService = new AuthService(propertyService, userService, sessionService);
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", "admin@admin.admin", Role.ADMIN);
        admin.setEmail("admin@admin.admin");
        userList = new ArrayList<>();
        userList.addAll(userService.findAll());
    }

    @After
    public void afterTest() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            userService.remove(user);
        }
    }

    @Test
    public void testAdd() throws Exception {
        int expectedNumberOfEntries = userService.getSize() + 1;
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("TEST_LOGIN");
        user.setFirstName("TEST_FIRSTNAME");
        user.setLastName("TEST_LASTNAME");
        user.setEmail("TEST@TEST.TEST");
        user.setRole(Role.USUAL);
        user.setPasswordHash("123");
        userService.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        userService.remove(user);
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final UserDTO user = userService.create(login, password);
        @NotNull final String userId = user.getId();
        @Nullable final UserDTO newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
    }

    @Test
    public void testCreateLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "PASS"));
    }

    @Test
    public void testCreateLoginExists() {
        Assert.assertThrows(LoginExistsException.class, () -> userService.create("USER", "PASS"));
    }

    @Test
    public void testCreatePasswordEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("LOGIN", ""));
    }

    @Test
    public void testCreateWithEmail() throws Exception {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final String email = "EMAIL";
        @NotNull final UserDTO user = userService.create(login, password, email);
        @NotNull final String userId = user.getId();
        @Nullable final UserDTO newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(email, newUser.getEmail());
    }

    @Test
    public void testCreateWithEmailLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "PASS", "EMAIL"));
    }

    @Test
    public void testCreateWithEmailLoginExists() {
        Assert.assertThrows(LoginExistsException.class, () -> userService.create("USER", "PASS", "EMAIL"));
    }

    @Test
    public void testCreateWithEmailPasswordEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("LOGIN", "", "EMAIL"));
    }

    @Test
    public void testCreateWithEmailEmailExists() {
        Assert.assertThrows(EmailExistsException.class, () -> userService.create("LOGIN", "PASS", "user@user.user"));
    }

    @Test
    public void testCreateWithRole() throws Exception {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final String email = "u@u.u";
        @NotNull final Role role = Role.USUAL;
        @NotNull final UserDTO user = userService.create(login, password, email, role);
        @NotNull final String userId = user.getId();
        @Nullable final UserDTO newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(role, newUser.getRole());
    }

    @Test
    public void testCreateWithRoleLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "PASS", "admin@admin.admin", Role.USUAL));
    }

    @Test
    public void testCreateWithRoleLoginExists() {
        Assert.assertThrows(LoginExistsException.class, () -> userService.create("USER", "PASS", "admin@admin.admin", Role.USUAL));
    }

    @Test
    public void testCreateWithRolePasswordEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("LOGIN", "", "admin@admin.admin", Role.USUAL));
    }

    @Test
    public void testExistById() {
        for (@NotNull final UserDTO user : userList) {
            final boolean isExist = userService.existsById(user.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdFalse() {
        final boolean isExist = userService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testFindAll() {
        @NotNull final List<UserDTO> users = userService.findAll();
        Assert.assertEquals(userList, users);
    }

    @Test
    public void testFindByLogin() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final UserDTO foundUser = userService.findByLogin(user.getLogin());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void testFindByLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
    }

    @Test
    public void testFindByLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
    }

    @Test
    public void testFindOneById() {
        @Nullable UserDTO foundUser;
        for (@NotNull final UserDTO user : userList) {
            foundUser = userService.findOneById(user.getId());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(null));
    }

    @Test
    public void testFindOneByIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(""));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 1; i <= userList.size(); i++) {
            @Nullable final UserDTO user = userService.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test
    public void testFindOneByIndexInvalid() {
        int index = userService.getSize() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(index));
    }

    @Test
    public void testFindOneByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(null));
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(-1));
    }

    @Test
    public void teatGetSize() {
        int expectedSize = userList.size();
        int actualSize = userService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testIsLoginExist() throws Exception {
        boolean isExist = userService.isLoginExist("ADMIN");
        Assert.assertTrue(isExist);
    }

    @Test
    public void testIsLoginExistFalse() throws Exception {
        boolean isExist = userService.isLoginExist("meow");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsLoginExistNull() throws Exception {
        boolean isExist = userService.isLoginExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsLoginExistEmpty() throws Exception {
        boolean isExist = userService.isLoginExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsEmailExist() throws Exception {
        boolean isExist = userService.isEmailExist("user@user.user");
        Assert.assertTrue(isExist);
    }

    @Test
    public void testIsEmailExistFalse() throws Exception {
        boolean isExist = userService.isEmailExist("meow@meow.meow");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsEmailExistNull() throws Exception {
        boolean isExist = userService.isEmailExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsEmailExistEmpty() throws Exception {
        boolean isExist = userService.isEmailExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testLockUserByLogin() throws Exception {
        userService.lockUserByLogin("USER");
        @Nullable final UserDTO user = userService.findByLogin("USER");
        Assert.assertNotNull(user);
        Assert.assertEquals(true, user.getLocked());
    }

    @Test
    public void testLockUserByLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
    }

    @Test
    public void testLockUserByLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
    }

    @Test
    public void testLockUserByLoginInvalid() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin("meow"));
    }

    @Test
    public void testRemoveByLogin() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin("USER");
        Assert.assertNotNull(user);
        @Nullable UserDTO deletedUser = userService.removeByLogin("USER");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByLogin("USER");
        Assert.assertNull(deletedUser);
    }

    @Test
    public void testRemoveByLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
    }

    @Test
    public void testRemoveByLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
    }

    @Test
    public void testRemoveByLoginInvalid() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("meow"));
    }

    @Test
    public void testRemoveByEmail() throws Exception {
        @Nullable final UserDTO user = userService.findByEmail("user@user.user");
        Assert.assertNotNull(user);
        @Nullable UserDTO deletedUser = userService.removeByEmail("user@user.user");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByEmail("user@user.user");
        Assert.assertNull(deletedUser);
    }

    @Test
    public void testRemoveByEmailNull() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(null));
    }

    @Test
    public void testRemoveByEmailEmpty() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(""));
    }

    @Test
    public void testRemoveByEmailNotFound() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByEmail("123"));
    }

    @Test
    public void testSetPassword() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin("USER");
        Assert.assertNotNull(user);
        @Nullable final UserDTO updatedUser = userService.setPassword(user.getId(), "NEW_PASS");
        Assert.assertNotNull(updatedUser);
        @Nullable final String token = authService.login("USER", "NEW_PASS");
        Assert.assertNotNull(token);
        authService.logout(token);
    }

    @Test
    public void testSetPasswordIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(null, "NEW_PASS"));
    }

    @Test
    public void testSetPasswordIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword("", "NEW_PASS"));
    }

    @Test
    public void testSetPasswordNull() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("ID", null));
    }

    @Test
    public void testSetPasswordEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("ID", ""));
    }

    @Test
    public void testSetPasswordNotFound() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.setPassword("meow", "NEW_PASS"));
    }

    @Test
    public void testRemove() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final UserDTO deletedUser = userService.remove(user);
            Assert.assertNotNull(deletedUser);
            @Nullable final UserDTO deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test
    public void testRemoveNull() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.remove(null));
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final UserDTO user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final UserDTO deletedUser = userService.removeById(userId);
            Assert.assertNotNull(deletedUser);
            @Nullable final UserDTO deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test
    public void testRemoveByIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(null));
    }

    @Test
    public void testRemoveByIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(""));
    }

    @Test
    public void testRemoveByIdNotFound() {
        Assert.assertThrows(EntityNotFoundException.class, () -> userService.removeById("meow"));
    }

    @Test
    public void testRemoveByIndexInvalid() {
        int index = userList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(index));
    }

    @Test
    public void testRemoveByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(null));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(-1));
    }

    @Test
    public void testUnlockUserByLogin() throws Exception {
        userService.lockUserByLogin("USER");
        @Nullable final UserDTO lockedUser = userService.findByLogin("USER");
        Assert.assertNotNull(lockedUser);
        Assert.assertEquals(true, lockedUser.getLocked());
        userService.unlockUserByLogin("USER");
        @Nullable final UserDTO unlockedUser = userService.findByLogin("USER");
        Assert.assertNotNull(unlockedUser);
        Assert.assertEquals(false, unlockedUser.getLocked());
    }

    @Test
    public void testUnlockUserByLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(null));
    }

    @Test
    public void testUnlockUserByLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(""));
    }

    @Test
    public void testUnlockUserByLoginNotFound() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin("meow"));
    }

    @Test
    public void testUpdateUser() throws Exception {
        @NotNull final String firstName = "NEW_FIRST_NAME";
        @NotNull final String lastName = "NEW_LAST_NAME";
        @NotNull final String middleName = "NEW_MIDDLE_NAME";
        @Nullable final UserDTO user = userService.findByLogin("USER");
        Assert.assertNotNull(user);
        userService.updateUser(user.getId(), firstName, lastName, middleName);
        @Nullable final UserDTO updatedUser = userService.findByLogin("USER");
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }

    @Test
    public void testUpdateUserIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(null, "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME"));
    }

    @Test
    public void testUpdateUserIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser("", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME"));
    }

    @Test
    public void testUpdateUserNotFound() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.updateUser("123321", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME"));
    }

}