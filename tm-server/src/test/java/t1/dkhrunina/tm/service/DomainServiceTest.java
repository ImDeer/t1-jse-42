package t1.dkhrunina.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;

import java.util.List;

public class DomainServiceTest {

    @NotNull
    private IServiceLocator serviceLocator;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private UserDTO user;

    @NotNull
    private UserDTO admin;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        serviceLocator = new IServiceLocator() {

            @Getter
            @NotNull
            final IPropertyService propertyService = new PropertyService();

            @Getter
            @NotNull
            final IProjectService projectService = new ProjectService(connectionService);

            @Getter
            @NotNull
            final ITaskService taskService = new TaskService(connectionService);

            @Getter
            @NotNull
            final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

            @Getter
            @NotNull
            final IUserService userService = new UserService(propertyService, connectionService, projectTaskService);

            @Getter
            @NotNull
            final IDomainService domainService = new DomainService(this);

            @Getter
            @NotNull
            final ISessionService sessionService = new SessionService(connectionService);

            @Getter
            @NotNull
            final IAuthService authService = new AuthService(propertyService, userService, sessionService);

            @Getter
            @NotNull
            final ILoggerService loggerService = new LoggerService(propertyService);
        };

        user = serviceLocator.getUserService().create("USER", "USER", "user@user.user");
        admin = serviceLocator.getUserService().create("ADMIN", "ADMIN", "admin@admin.admin", Role.ADMIN);
        @NotNull final ProjectDTO project1 = serviceLocator.getProjectService().create(user.getId(), "project 1", "user project 1");
        @NotNull final ProjectDTO project2 = serviceLocator.getProjectService().create(user.getId(), "project 2", "user project 2");
        @NotNull final ProjectDTO project3 = serviceLocator.getProjectService().create(admin.getId(), "project 3", "admin project");
        @NotNull final TaskDTO task1 = serviceLocator.getTaskService().create(user.getId(), "task 1", "project 1 task");
        task1.setProjectId(project1.getId());
        serviceLocator.getTaskService().update(task1);
        @NotNull final TaskDTO task2 = serviceLocator.getTaskService().create(user.getId(), "task 2", "project 2 task");
        task2.setProjectId(project2.getId());
        serviceLocator.getTaskService().update(task2);
        @NotNull final TaskDTO task3 = serviceLocator.getTaskService().create(admin.getId(), "task 3", "project 3 task");
        task3.setProjectId(project3.getId());
        serviceLocator.getTaskService().update(task3);
    }

    @After
    public void afterTest() throws Exception {
        serviceLocator.getTaskService().clear(user.getId());
        serviceLocator.getTaskService().clear(admin.getId());
        serviceLocator.getProjectService().clear(user.getId());
        serviceLocator.getProjectService().clear(admin.getId());
        serviceLocator.getUserService().remove(user);
        serviceLocator.getUserService().remove(admin);
    }

    @Test
    public void testDataBackup() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<UserDTO> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBackup();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBackup();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataBase64() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<UserDTO> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBase64();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBase64();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataBinary() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<UserDTO> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBinary();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBinary();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataJsonFasterXml() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<UserDTO> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonFasterXml();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataJsonFasterXml();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataJsonJaxB() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<UserDTO> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonJaxB();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataJsonJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataXmlFasterXml() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<UserDTO> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXmlFasterXml();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataXmlFasterXml();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataXmlJaxB() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<UserDTO> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXmlJaxB();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataXmlJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataYamlFasterXml() throws Exception {
        @Nullable final List<ProjectDTO> projects = serviceLocator.getProjectService().findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskService().findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<UserDTO> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataYamlFasterXml();
        serviceLocator.getTaskService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize(user.getId()));
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataYamlFasterXml();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll(user.getId()));
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll(user.getId()));
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

}