package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.entity.ProjectNotFoundException;
import t1.dkhrunina.tm.exception.entity.TaskNotFoundException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.exception.field.ProjectIdEmptyException;
import t1.dkhrunina.tm.exception.field.TaskIdEmptyException;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectTaskServiceTest {

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private UserDTO user;

    @NotNull
    private UserDTO admin;

    @NotNull
    private IUserService userService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        projectList = new ArrayList<>();
        projectService.clear();
        taskService.clear();
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", "admin@admin.admin", Role.ADMIN);
        @NotNull final ProjectDTO project1 = projectService.create(user.getId(), "project 1", "user project 1");
        @NotNull final ProjectDTO project2 = projectService.create(user.getId(), "project 2", "user project 2");
        @NotNull final ProjectDTO project3 = projectService.create(admin.getId(), "project 3", "admin project");
        @NotNull final TaskDTO task1 = taskService.create(user.getId(), "task 1", "project 1 task");
        task1.setProjectId(project1.getId());
        taskService.update(task1);
        @NotNull final TaskDTO task2 = taskService.create(user.getId(), "task 2", "project 2 task");
        task2.setProjectId(project2.getId());
        taskService.update(task2);
        @NotNull final TaskDTO task3 = taskService.create(admin.getId(), "task 3", "project 3 task");
        task3.setProjectId(project3.getId());
        taskService.update(task3);
        projectList.add(project1);
        projectList.add(project2);
        projectList.add(project3);
    }

    @After
    public void afterTest() throws Exception {
        taskService.clear(user.getId());
        taskService.clear(admin.getId());
        projectService.clear(user.getId());
        projectService.clear(admin.getId());
        userService.remove(user);
        userService.remove(admin);
    }

    @Test
    public void testBindTaskToProject() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @NotNull final ProjectDTO project = projects.get(0);
        int expectedNumberOfEntries = taskService.findAllByProjectId(user.getId(), project.getId()).size() + 1;
        @NotNull final TaskDTO newTask = taskService.create(user.getId(), "Test name", "Test description");
        @NotNull final TaskDTO boundTask = projectTaskService.bindTaskToProject(user.getId(), project.getId(), newTask.getId());
        Assert.assertNotNull(boundTask);
        int newNumberOfEntries = taskService.findAllByProjectId(user.getId(), project.getId()).size();
        Assert.assertEquals(expectedNumberOfEntries, newNumberOfEntries);
    }

    @Test
    public void testBindTaskToProjectProjectIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), null, "TASK_ID"));
    }

    @Test
    public void testBindTaskToProjectProjectIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "", "TASK_ID"));
    }

    @Test
    public void testBindTaskToProjectTaskIdNull() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "PROJECT_ID", null));
    }

    @Test
    public void testBindTaskToProjectTaskIdEmpty() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "PROJECT_ID", ""));
    }

    @Test
    public void testBindTaskToProjectProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "PROJECT_ID", "123321"));
    }

    @Test
    public void testBindTaskToProjectTaskNotFound() {
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), projectId, "123321"));
    }

    @Test
    public void testRemoveProjectById() throws Exception {
        @NotNull final List<ProjectDTO> projects = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        for (@NotNull final ProjectDTO project : projects) {
            projectTaskService.removeProjectById(user.getId(), project.getId());
        }
        @Nullable final List<ProjectDTO> projectsAfterRemoving = projectService.findAll(user.getId());
        Assert.assertNotNull(projectsAfterRemoving);
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @Nullable final List<TaskDTO> tasksAfterRemoving = taskService.findAll(user.getId());
        Assert.assertNotNull(tasksAfterRemoving);
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test
    public void testRemoveProjectByIdProjectIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(user.getId(), null));
    }

    @Test
    public void testRemoveProjectByIdProjectIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(user.getId(), ""));
    }

    @Test
    public void testRemoveProjectByIdProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(user.getId(), "123321"));
    }

    @Test
    public void testRemoveProjectByIndex() throws Exception {
        @NotNull final List<ProjectDTO> projects = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        int index = projects.size();
        while (index > 0) {
            projectTaskService.removeProjectByIndex(user.getId(), index);
            index--;
        }
        @Nullable final List<ProjectDTO> projectsAfterRemoving = projectService.findAll(user.getId());
        Assert.assertNotNull(projectsAfterRemoving);
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @Nullable final List<TaskDTO> tasksAfterRemoving = taskService.findAll(user.getId());
        Assert.assertNotNull(tasksAfterRemoving);
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test
    public void testRemoveProjectByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectTaskService.removeProjectByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveProjectByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectTaskService.removeProjectByIndex(user.getId(), -1));
    }

    @Test
    public void testRemoveProjectByIndexInvalid() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectTaskService.removeProjectByIndex(user.getId(), projectList.size() + 1));
    }

    @Test
    public void testClearProject() throws Exception {
        int numberOfProjects = projectService.getSize(user.getId());
        Assert.assertTrue(numberOfProjects > 0);
        int numberOfTasks = taskService.getSize(user.getId());
        Assert.assertTrue(numberOfTasks > 0);
        projectTaskService.removeAllByUserId(user.getId());
        int numberOfProjectsAfterRemoving = projectService.getSize(user.getId());
        Assert.assertEquals(0, numberOfProjectsAfterRemoving);
        int numberOfTasksAfterRemoving = taskService.getSize(user.getId());
        Assert.assertEquals(0, numberOfTasksAfterRemoving);
    }

    @Test
    public void testUnbindTaskFromProject() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @NotNull final ProjectDTO project = projects.get(0);
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(user.getId(), project.getId());
        Assert.assertNotNull(tasks);
        @NotNull final String taskForUnbindId = tasks.get(0).getId();
        @Nullable final TaskDTO boundTask = projectTaskService.bindTaskToProject(user.getId(), project.getId(), taskForUnbindId);
        Assert.assertNotNull(boundTask);
        @Nullable TaskDTO unboundTask = projectTaskService.unbindTaskFromProject(user.getId(), boundTask.getProjectId(), boundTask.getId());
        Assert.assertNotNull(unboundTask);
        unboundTask = taskService.findOneById(user.getId(), unboundTask.getId());
        Assert.assertNotNull(unboundTask);
        Assert.assertNull(unboundTask.getProjectId());
    }

    @Test
    public void testUnbindTaskProjectIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), null, "TASK_ID"));
    }

    @Test
    public void testUnbindTaskProjectIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), "", "TASK_ID"));
    }

    @Test
    public void testUnbindTaskTaskIdNull() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), "PROJECT_ID", null));
    }

    @Test
    public void testUnbindTaskTaskIdEmpty() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), "PROJECT_ID", ""));
    }

    @Test
    public void testUnbindTaskProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, ()->projectTaskService.unbindTaskFromProject(user.getId(), "PROJECT_ID", "123321"));
    }

    @Test
    public void testUnbindTaskTaskNotFound() {
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, ()->projectTaskService.unbindTaskFromProject(user.getId(), projectId, "123321"));
    }

}