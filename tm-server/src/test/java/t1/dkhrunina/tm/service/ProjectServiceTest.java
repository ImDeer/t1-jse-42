package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.entity.ProjectNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectServiceTest {

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private UserDTO user;

    @NotNull
    private UserDTO admin;

    @NotNull
    private IUserService userService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        projectList = new ArrayList<>();
        userService.clear();
        projectService.clear();
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", "admin@admin.admin", Role.ADMIN);
        @NotNull final ProjectDTO project1 = projectService.create(user.getId(), "project 1", "user project 1");
        @NotNull final ProjectDTO project2 = projectService.create(user.getId(), "project 2", "user project 2");
        @NotNull final ProjectDTO project3 = projectService.create(admin.getId(), "project 3", "admin project");
        projectList.add(project1);
        projectList.add(project2);
        projectList.add(project3);
    }

    @After
    public void afterTest() throws Exception {
        userService.remove(user);
        userService.remove(admin);
    }

    @Test
    public void testAddForUser() throws Exception {
        int expectedNumberOfEntries = projectService.getSize(user.getId()) + 1;
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getId());
        project.setName("Test Add");
        project.setDescription("Test Add");
        projectService.add(user.getId(), project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testAddNullForUser() throws Exception {
        int expectedNumberOfEntries = projectService.getSize(user.getId());
        @Nullable final ProjectDTO project = projectService.add(user.getId(), null);
        Assert.assertNull(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testChangeProjectStatusById() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final String projectId = project.getId();
            @Nullable ProjectDTO changedProject = projectService.changeProjectStatusById(user.getId(), projectId, Status.IN_PROGRESS);
            Assert.assertNotNull(changedProject);
            changedProject = projectService.findOneById(user.getId(), projectId);
            Assert.assertNotNull(changedProject);
            Assert.assertEquals(Status.IN_PROGRESS, changedProject.getStatus());
        }
    }

    @Test
    public void testChangeProjectStatusByIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.changeProjectStatusById(user.getId(), "", Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.changeProjectStatusById(user.getId(), null, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIdProjectInvalid() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusById(user.getId(), "123", Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIdStatusIncorrect() {
        Assert.assertThrows(StatusIncorrectException.class, () -> projectService.changeProjectStatusById(user.getId(), "123", null));
    }

    @Test
    public void testChangeProjectStatusByIndex() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        for (int i = 1; i <= projects.size(); i++) {
            @NotNull final ProjectDTO project = projects.get(i - 1);
            @NotNull final String projectId = project.getId();
            @Nullable ProjectDTO changedProject = projectService.changeProjectStatusByIndex(user.getId(), i, Status.IN_PROGRESS);
            Assert.assertNotNull(changedProject);
            changedProject = projectService.findOneById(user.getId(), projectId);
            Assert.assertNotNull(changedProject);
            Assert.assertEquals(Status.IN_PROGRESS, changedProject.getStatus());
        }
    }

    @Test
    public void testChangeProjectStatusByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(user.getId(), null, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(user.getId(), -1, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIndexInvalid() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(user.getId(), 100, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIndexStatusIncorrect() {
        Assert.assertThrows(StatusIncorrectException.class, () -> projectService.changeProjectStatusByIndex(user.getId(), 1, null));
    }

    @Test
    public void testClearForUser() throws Exception {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(projectService.getSize(user.getId()) > 0);
        projectService.clear(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testCreate() {
        int expectedNumberOfEntries = projectService.getSize(user.getId()) + 1;
        @NotNull final String name = "Project Name";
        @NotNull final String description = "Project Description";
        @Nullable ProjectDTO createdProject = projectService.create(user.getId(), name, description);
        @NotNull final String projectId = createdProject.getId();
        Assert.assertNotNull(createdProject);
        createdProject = projectService.findOneById(user.getId(), projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(description, createdProject.getDescription());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testCreateByName() {
        int expectedNumberOfEntries = projectService.getSize(user.getId()) + 1;
        @NotNull final String name = "Project name";
        @Nullable ProjectDTO createdProject = projectService.create(user.getId(), name);
        @NotNull final String projectId = createdProject.getId();
        Assert.assertNotNull(createdProject);
        createdProject = projectService.findOneById(user.getId(), projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testCreateNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(user.getId(), "", "description"));
    }

    @Test
    public void testCreateNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(user.getId(), null, "description"));
    }

    @Test
    public void testCreateDescriptionEmpty() {
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(user.getId(), "name", ""));
    }

    @Test
    public void testCreateDescriptionNull() {
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(user.getId(), "name", null));
    }

    @Test
    public void testCreateNameEmptyDescriptionNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(user.getId(), ""));
    }

    @Test
    public void testCreateByNameNullDescriptionNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(user.getId(), null));
    }

    @Test
    public void testExistByIdForUser() throws Exception {
        @NotNull final List<ProjectDTO> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final ProjectDTO project : projectsForTestUser) {
            final boolean isExist = projectService.existsById(user.getId(), project.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdForUserInvalid() throws Exception {
        final boolean isExist = projectService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testFindAll() {
        @Nullable final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    public void testFindAllForUser() throws Exception {
        @NotNull final List<ProjectDTO> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable final List<ProjectDTO> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUser() {
        @NotNull final List<ProjectDTO> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable Comparator<ProjectDTO> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<ProjectDTO> projects = projectService.findAll(user.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectService.findAll(user.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectService.findAll(user.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
    }

    @Test
    public void testFindAllWithSortForUser() {
        @NotNull final List<ProjectDTO> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable List<ProjectDTO> projects = projectService.findAll(user.getId(), Sort.BY_NAME);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        projects = projectService.findAll(user.getId(), Sort.BY_CREATED);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        projects = projectService.findAll(user.getId(), Sort.BY_STATUS);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        projects = projectService.findAll(user.getId(), (@Nullable Sort) null);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable ProjectDTO foundProject;
        @NotNull final List<ProjectDTO> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final ProjectDTO project : projectsForTestUser) {
            foundProject = projectService.findOneById(user.getId(), project.getId());
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void testFindOneByIdNullForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(user.getId(), null));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<ProjectDTO> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= projectsForTestUser.size(); i++) {
            @Nullable final ProjectDTO project = projectService.findOneByIndex(user.getId(), i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void testFindOneByIndexInvalidForUser() {
        int index = projectService.getSize(user.getId()) + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(user.getId(), index));
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(user.getId(), null));
    }

    @Test
    public void testFindOneByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(user.getId(), -1));
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .count();
        int actualSize = projectService.getSize(user.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemoveByIdForUser() throws Exception {
        @NotNull final List<ProjectDTO> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final ProjectDTO project : projectsForTestUser) {
            @NotNull final String projectId = project.getId();
            @Nullable final ProjectDTO deletedProject = projectService.removeById(user.getId(), projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final ProjectDTO deletedProjectInRepository = projectService.findOneById(user.getId(), projectId);
            Assert.assertNull(deletedProjectInRepository);
        }
    }

    @Test
    public void testRemoveByIdNullForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(user.getId(), null));
    }

    @Test
    public void testRemoveByIdEmptyForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(user.getId(), ""));
    }

    @Test
    public void testRemoveByIdForUserNotFound() {
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.removeById(user.getId(), "123321"));
    }

    @Test
    public void testRemoveByIndexForUser() throws Exception {
        int index = (int) projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final ProjectDTO deletedProject = projectService.removeByIndex(user.getId(), index);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            @Nullable final ProjectDTO deletedProjectInRepository = projectService.findOneById(user.getId(), projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexIncorrectForUser() {
        int index = projectList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(user.getId(), index));
    }

    @Test
    public void testRemoveByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(user.getId(), -1));
    }

    @Test
    public void testUpdateById() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        @NotNull String name;
        @NotNull String description;
        int index = 0;
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final String projectId = project.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable ProjectDTO updatedProject = projectService.updateById(user.getId(), projectId, name, description);
            Assert.assertNotNull(updatedProject);
            updatedProject = projectService.findOneById(user.getId(), projectId);
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals(name, updatedProject.getName());
            Assert.assertEquals(description, updatedProject.getDescription());
            index++;
        }
    }

    @Test
    public void testUpdateByIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(user.getId(), "", "name", "description"));
    }

    @Test
    public void testUpdateByIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(user.getId(), null, "name", "description"));
    }

    @Test
    public void testUpdateByIdNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(user.getId(), "id", "", "description"));
    }

    @Test
    public void testUpdateByIdNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(user.getId(), "id", null, "description"));
    }

    @Test
    public void testUpdateByIdProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateById(user.getId(), "123", "name", "description"));
    }

    @Test
    public void testUpdateByIndex() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectService.findAll(user.getId());
        Assert.assertNotNull(projects);
        @NotNull String name;
        @NotNull String description;
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final String projectId = project.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable ProjectDTO updatedProject = projectService.updateByIndex(user.getId(), index, name, description);
            Assert.assertNotNull(updatedProject);
            updatedProject = projectService.findOneById(user.getId(), projectId);
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals(name, updatedProject.getName());
            Assert.assertEquals(description, updatedProject.getDescription());
            index++;
        }
    }

    @Test
    public void testUpdateByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(user.getId(), null, "name", "description"));
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(user.getId(), -1, "name", "description"));
    }

    @Test
    public void testUpdateByIndexIncorrect() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(user.getId(), 100, "name", "description"));
    }

    @Test
    public void testUpdateByIndexNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(user.getId(), 1, null, "description"));
    }

    @Test
    public void testUpdateByIndexNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(user.getId(), 1, "", "description"));
    }

}