package t1.dkhrunina.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.UserDTO;

import java.util.List;

@CacheNamespace
public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, created, login, password, email, " +
            "first_name, last_name, middle_name, role, locked)" +
            "VALUES (#{id}, #{created}, #{login}, #{passwordHash}, #{email}, " +
            "#{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void add(@NotNull UserDTO user);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Select("SELECT * FROM tm_user ORDER BY created DESC")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable List<UserDTO> findAll();

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findByLogin(@Nullable @Param("login") String login) throws Exception;

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findByEmail(@Nullable @Param("email") String email) throws Exception;

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findOneById(@Nullable @Param("id") String id);

    @Select("SELECT * FROM tm_user ORDER BY created DESC LIMIT 1 OFFSET #{index} - 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable UserDTO findOneByIndex(@Nullable @Param("index") Integer index);

    @Select("SELECT COUNT(id) FROM tm_user")
    int getSize();

    @Delete("DELETE  FROM tm_user WHERE id = #{id}")
    void remove(@Nullable UserDTO user);

    @Update("UPDATE tm_user SET id = #{id}, created = #{created}, login = #{login}, password = #{passwordHash}," +
            "email = #{email}, first_name = #{firstName}, last_name = #{lastName}, middle_name = #{middleName}, " +
            "role = #{role}, locked = #{locked} WHERE id = #{id}")
    void update(@NotNull UserDTO user);

}