package t1.dkhrunina.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.ProjectDTO;

import java.util.List;

@CacheNamespace
public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, created, status, description, user_id) " +
            "VALUES (#{id}, #{name}, #{created}, #{status}, #{description}, #{userId})")
    void add(@NotNull ProjectDTO project);

    @Insert("INSERT INTO tm_project (id, name, created, status, description, user_id) " +
            "VALUES (#{id}, #{name}, #{created}, #{status}, #{description}, #{userId})")
    void addForUser(@NotNull @Param("userId") String userId, @NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Select("SELECT * FROM tm_project ORDER BY created DESC")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable List<ProjectDTO> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created DESC")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable List<ProjectDTO> findAllForUser(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable List<ProjectDTO> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable List<ProjectDTO> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable List<ProjectDTO> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable ProjectDTO findOneById(@NotNull @Param("userId") String userId, @Nullable @Param("id") String id);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created DESC LIMIT 1 OFFSET #{index} - 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable ProjectDTO findOneByIndex(@NotNull @Param("userId") String userId, @Nullable @Param("index") Integer index);

    @Select("SELECT COUNT(id) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE  FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void remove(@Nullable ProjectDTO project);

    @Update("UPDATE tm_project SET name = #{name}, description = #{description}, status = #{status}" +
            "WHERE user_id = #{userId} AND id = #{id}")
    void update(@NotNull ProjectDTO project);

}