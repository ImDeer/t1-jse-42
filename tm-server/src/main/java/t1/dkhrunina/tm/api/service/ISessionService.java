package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionService extends IUserOwnedService<SessionDTO> {

    SessionDTO remove(@Nullable SessionDTO session);

    List<SessionDTO> findAll();

}