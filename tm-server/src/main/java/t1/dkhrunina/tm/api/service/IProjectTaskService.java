package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.TaskDTO;

public interface IProjectTaskService {

    @NotNull
    TaskDTO bindTaskToProject(@NotNull String userId, @Nullable String projectId,
                              @Nullable String taskId) throws Exception;

    void removeAllByUserId(@NotNull String userId) throws Exception;

    void removeProjectById(@NotNull String userId, @Nullable String projectId) throws Exception;

    void removeProjectByIndex(@NotNull String userId, @Nullable Integer index) throws Exception;

    @NotNull
    TaskDTO unbindTaskFromProject(@NotNull String userId, @Nullable String projectId,
                                  @Nullable String taskId) throws Exception;

}