package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.SessionDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.enumerated.Role;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable String token);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    void invalidate(@Nullable SessionDTO session) throws Exception;

    @NotNull
    UserDTO register(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

    @NotNull
    UserDTO register(
            @NotNull String login,
            @NotNull String password,
            @Nullable String email,
            @Nullable Role role
    ) throws Exception;

}