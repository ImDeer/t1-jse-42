package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> {

    M add(@NotNull String userId, @Nullable M model) throws Exception;

    void clear(@NotNull String userId) throws Exception;

    boolean existsById(@NotNull String userId, @Nullable String id) throws Exception;

    @Nullable
    List<M> findAll(@NotNull String userId) throws Exception;

    @Nullable
    M findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @Nullable Integer index);

    int getSize(@NotNull String userId);

    @Nullable
    M removeById(@NotNull String userId, @Nullable String id) throws Exception;

    @Nullable
    M removeByIndex(@NotNull String userId, @Nullable Integer index) throws Exception;

}