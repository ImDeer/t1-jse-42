package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.dto.model.AbstractModelDTO;

import java.util.List;

public interface IService<M extends AbstractModelDTO> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

}