package t1.dkhrunina.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.IProjectTaskService;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.exception.entity.ProjectNotFoundException;
import t1.dkhrunina.tm.exception.entity.TaskNotFoundException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.exception.field.ProjectIdEmptyException;
import t1.dkhrunina.tm.exception.field.TaskIdEmptyException;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public TaskDTO bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                boolean isExist = projectRepository.findOneById(userId, projectId) != null;
                if (!isExist) throw new ProjectNotFoundException();
                task = taskRepository.findOneById(userId, taskId);
                if (task == null) throw new TaskNotFoundException();
                task.setProjectId(projectId);
                taskRepository.update(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return task;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @Nullable final List<ProjectDTO> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                taskRepository.clearForUser(userId);
                projects = projectRepository.findAllForUser(userId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        if (projects == null) return;
        for (@NotNull final ProjectDTO project : projects) removeProjectById(userId, project.getId());
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable ProjectDTO project;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                project = projectRepository.findOneById(userId, projectId);
                if (project == null) throw new ProjectNotFoundException();
                taskRepository.removeAllByProjectId(userId, projectId);
                projectRepository.remove(project);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        @Nullable final ProjectDTO project;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                if (index > projectRepository.getSize(userId)) throw new IndexIncorrectException();
                project = projectRepository.findOneByIndex(userId, index);
                if (project == null) throw new ProjectNotFoundException();
                removeProjectById(userId, project.getId());
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @NotNull
    @Override
    public TaskDTO unbindTaskFromProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                boolean isExist = projectRepository.findOneById(userId, projectId) != null;
                if (!isExist) throw new ProjectNotFoundException();
                task = taskRepository.findOneById(userId, taskId);
                if (task == null) throw new TaskNotFoundException();
                task.setProjectId(null);
                taskRepository.update(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return task;
    }

}