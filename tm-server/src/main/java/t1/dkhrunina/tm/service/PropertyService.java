package t1.dkhrunina.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_NAME_KEY = "application.name";

    @NotNull
    public static final String APPLICATION_NAME_DEFAULT = "task-manager";

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    public static final String APPLICATION_LOG_KEY = "application.log";

    @NotNull
    public static final String APPLICATION_LOG_DEFAULT = "./";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String DB_DIALECT_KEY = "database.dialect";

    @NotNull
    public static final String DB_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DB_HBM2DDL_KEY = "database.hbm2ddlauto";

    @NotNull
    private static final String DB_HBM2DDL_DEFAULT = "validate";

    @NotNull
    public static final String DB_USER_KEY = "database.username";

    @NotNull
    public static final String DB_USER_DEFAULT = "postgres";

    @NotNull
    public static final String DB_PASSWORD_KEY = "database.password";

    @NotNull
    public static final String DB_PASSWORD_DEFAULT = "postgres";

    @NotNull
    private static final String DB_LOGGING_ENABLED_KEY = "database.loggingEnabled";

    @NotNull
    private static final String DB_LOGGING_ENABLED_DEFAULT = "false";

    @NotNull
    private static final String DB_SECOND_LEVEL_CACHE_ENABLED_KEY = "database.secondLevelCacheEnabled";

    @NotNull
    private static final String DB_SECOND_LEVEL_CACHE_ENABLED_DEFAULT = "false";

    @NotNull
    public static final String DB_URL_KEY = "database.url";

    @NotNull
    public static final String GIT_BRANCH_KEY = "gitBranch";

    @NotNull
    public static final String GIT_COMMIT_ID_KEY = "gitCommitId";

    @NotNull
    public static final String GIT_COMMIT_MESSAGE_KEY = "gitCommitMessage";

    @NotNull
    public static final String GIT_COMMIT_TIME_KEY = "gitCommitTime";

    @NotNull
    public static final String GIT_COMMITTER_NAME_KEY = "gitCommiterName";

    @NotNull
    public static final String GIT_COMMITTER_EMAIL_KEY = "gitCommiterEmail";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "1980";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "Quack7";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String SERVER_PORT = "server.port";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    public static final String SERVER_HOST = "server.host";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_KEY_DEFAULT = "777";

    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    public static final String SESSION_TIMEOUT_DEFAULT = "3600";

    @NotNull
    public static final String EMPTY_VALUE = "--//--";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    @SuppressWarnings("IOStreamConstructor")
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        boolean isEmpty = Manifests.exists(key);
        if (!isEmpty) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    private Boolean getBooleanValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Boolean.parseBoolean(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @SuppressWarnings("SameParameterValue")
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @Override
    public @NotNull String getApplicationLog() {
        return getStringValue(APPLICATION_LOG_KEY, APPLICATION_LOG_DEFAULT);
    }

    @Override
    public @NotNull String getApplicationName() {
        return getStringValue(APPLICATION_NAME_KEY, APPLICATION_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @Override
    public @NotNull String getDBDialect() {
        return getStringValue(DB_DIALECT_KEY);
    }

    @NotNull
    @Override
    public String getDBDriver() {
        return getStringValue(DB_DRIVER_KEY);
    }

    @Override
    public @NotNull String getDBHbm2DdlAuto() {
        return getStringValue(DB_HBM2DDL_KEY, DB_HBM2DDL_DEFAULT);
    }

    @NotNull
    @Override
    public Boolean getDBLoggingEnabled() {
        return getBooleanValue(DB_LOGGING_ENABLED_KEY, DB_LOGGING_ENABLED_DEFAULT);
    }

    @NotNull
    @Override
    public Boolean getDBSecondLevelCacheEnabled() {
        return getBooleanValue(DB_SECOND_LEVEL_CACHE_ENABLED_KEY, DB_SECOND_LEVEL_CACHE_ENABLED_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DB_PASSWORD_KEY, DB_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DB_URL_KEY);
    }

    @NotNull
    @Override
    public String getDBUser() {
        return getStringValue(DB_USER_KEY, DB_USER_DEFAULT);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH_KEY);
    }

    @NotNull
    @Override
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID_KEY);
    }

    @NotNull
    @Override
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE_KEY);
    }

    @NotNull
    @Override
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME_KEY);
    }

    @NotNull
    @Override
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME_KEY);
    }

    @NotNull
    @Override
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public @NotNull String getServerPort() {
        return getStringValue(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

    @Override
    public @NotNull String getServerHost() {
        return getStringValue(SERVER_HOST, SERVER_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @Override
    public @NotNull Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
    }

}