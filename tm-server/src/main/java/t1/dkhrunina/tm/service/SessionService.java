package t1.dkhrunina.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.ISessionRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.ISessionService;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.IdEmptyException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.dto.model.SessionDTO;

import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public SessionDTO add(@NotNull final String userId, @Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setUserId(userId);
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                repository.add(session);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return session;
    }

    public void clear(@NotNull final String userId) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                repository.clearForUser(userId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            result = repository.findOneById(userId, id) != null;
        }
        return result;
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@NotNull final String userId) {
        @Nullable final List<SessionDTO> sessions;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            sessions = repository.findAllForUser(userId);
        }
        return sessions;
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO session;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneById(userId, id);
        }
        return session;
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final SessionDTO session;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneByIndex(userId, index);
        }
        return session;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        int result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            result = repository.getSize(userId);
        }
        return result;
    }

    @NotNull
    @Override
    public SessionDTO removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO removedSession;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                removedSession = repository.findOneById(userId, id);
                if (removedSession == null) throw new EntityNotFoundException();
                repository.remove(removedSession);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return removedSession;
    }

    @NotNull
    @Override
    public SessionDTO removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final SessionDTO removedSession;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                removedSession = repository.findOneByIndex(userId, index);
                if (removedSession == null) throw new EntityNotFoundException();
                repository.remove(removedSession);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return removedSession;
    }

    @NotNull
    @Override
    public SessionDTO remove(@Nullable final SessionDTO session) {
        if (session == null) throw new EntityNotFoundException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                repository.remove(session);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return session;
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @Nullable final List<SessionDTO> sessions;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            sessions = repository.findAll();
        }
        return sessions;
    }

}

