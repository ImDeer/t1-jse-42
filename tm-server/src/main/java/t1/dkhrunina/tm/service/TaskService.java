package t1.dkhrunina.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.ITaskService;
import t1.dkhrunina.tm.comparator.CreatedComparator;
import t1.dkhrunina.tm.comparator.NameComparator;
import t1.dkhrunina.tm.comparator.StatusComparator;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.entity.TaskNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public TaskDTO add(@NotNull final String userId, @Nullable final TaskDTO task) {
        if (task == null) return null;
        task.setUserId(userId);
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
                repository.add(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return task;
    }

    @Override
    public void clear(@NotNull final String userId) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
                repository.clearForUser(userId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            result = repository.findOneById(userId, id) != null;
            sqlSession.commit();
        }
        return result;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        @Nullable final List<TaskDTO> tasks;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAllForUser(userId);
        }
        return tasks;
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneById(userId, id);
        }
        return task;
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO task;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIndex(userId, index);
        }
        return task;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        int result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            result = repository.getSize(userId);
        }
        return result;
    }

    @NotNull
    @Override
    public TaskDTO removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO removedTask;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
                removedTask = repository.findOneById(userId, id);
                if (removedTask == null) throw new EntityNotFoundException();
                repository.remove(removedTask);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return removedTask;
    }

    @NotNull
    @Override
    public TaskDTO removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO removedTask;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
                removedTask = repository.findOneByIndex(userId, index);
                if (removedTask == null) throw new EntityNotFoundException();
                repository.remove(removedTask);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return removedTask;
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDTO create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @Override
    public void clear() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
                repository.clear();
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<TaskDTO> tasks;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAllByProjectId(userId, projectId);
        }
        return tasks;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @Nullable final List<TaskDTO> tasks;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            tasks = repository.findAll();
        }
        return tasks;
    }

    @Nullable
    @Override
    @SuppressWarnings("rawtypes")
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<TaskDTO> tasks = null;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            if (comparator == null)
                tasks = findAll(userId);
            else if (comparator == CreatedComparator.INSTANCE)
                tasks = repository.findAllOrderByCreated(userId);
            else if (comparator == NameComparator.INSTANCE)
                tasks = repository.findAllOrderByName(userId);
            else if (comparator == StatusComparator.INSTANCE)
                tasks = repository.findAllOrderByStatus(userId);
        }
        return tasks;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<TaskDTO> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
                repository.removeAllByProjectId(userId, projectId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @NotNull
    @Override
    public Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> tasks) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
                repository.clear();
                for (@NotNull final TaskDTO task : tasks)
                    repository.add(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return tasks;
    }

    @Override
    public void update(@NotNull final TaskDTO task) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
                repository.update(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @NotNull
    @Override
    public TaskDTO updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

}