package t1.dkhrunina.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.mybatis.caches.hazelcast.LoggingHazelcastCache;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ISessionRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.SessionDTO;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.Session;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.model.User;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.sqlSessionFactory = getSqlSessionFactory();
        this.entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public SqlSession getSQLSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final Boolean loggingEnabled = propertyService.getDBLoggingEnabled();
        if (loggingEnabled) LogFactory.useStdOutLogging();
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final String url = propertyService.getDBUrl();
        @NotNull final String driver = propertyService.getDBDriver();
        @NotNull final Boolean secondLevelCacheEnabled = propertyService.getDBSecondLevelCacheEnabled();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("tm", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.setCacheEnabled(secondLevelCacheEnabled);
        configuration.addCache(new LoggingHazelcastCache("hz"));
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IUserRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    private EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final String url = propertyService.getDBUrl();
        @NotNull final String driver = propertyService.getDBDriver();
        @NotNull final String dialect = propertyService.getDBDialect();
        @NotNull final String hbm2DdlAuto = propertyService.getDBHbm2DdlAuto();
        @NotNull final Boolean showSql = propertyService.getDBLoggingEnabled();
        settings.put(org.hibernate.cfg.Environment.DRIVER, driver);
        settings.put(org.hibernate.cfg.Environment.URL, url);
        settings.put(org.hibernate.cfg.Environment.USER, username);
        settings.put(org.hibernate.cfg.Environment.PASS, password);
        settings.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, hbm2DdlAuto);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, showSql.toString());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry standardServiceRegistry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(standardServiceRegistry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}