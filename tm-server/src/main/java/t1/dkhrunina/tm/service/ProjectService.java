package t1.dkhrunina.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.IProjectService;
import t1.dkhrunina.tm.comparator.CreatedComparator;
import t1.dkhrunina.tm.comparator.NameComparator;
import t1.dkhrunina.tm.comparator.StatusComparator;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.entity.ProjectNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public ProjectDTO add(@NotNull final String userId, @Nullable final ProjectDTO project) {
        if (project == null) return null;
        project.setUserId(userId);
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.add(project);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return project;
    }

    @Override
    public void clear(@NotNull final String userId) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.clearForUser(userId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                result = repository.findOneById(userId, id) != null;
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return result;
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @Nullable final List<ProjectDTO> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                projects = repository.findAllForUser(userId);
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return projects;
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) throw new IdEmptyException();
        @Nullable final ProjectDTO project;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                project = repository.findOneById(userId, id);
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final ProjectDTO project;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                project = repository.findOneByIndex(userId, index);
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return project;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        int result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                result = repository.getSize(userId);
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return result;
    }

    @NotNull
    @Override
    public ProjectDTO removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO removedProject;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                removedProject = repository.findOneById(userId, id);
                if (removedProject == null) throw new EntityNotFoundException();
                repository.remove(removedProject);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return removedProject;
    }

    @NotNull
    @Override
    public ProjectDTO removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final ProjectDTO removedProject;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                removedProject = repository.findOneByIndex(userId, index);
                if (removedProject == null) throw new EntityNotFoundException();
                repository.remove(removedProject);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return removedProject;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @Override
    public void clear() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.clear();
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        @Nullable final List<ProjectDTO> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                projects = repository.findAll();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return projects;
    }

    @Nullable
    @Override
    @SuppressWarnings("rawtypes")
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<ProjectDTO> projects = null;
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                if (comparator == null)
                    projects = findAll(userId);
                else if (comparator == CreatedComparator.INSTANCE)
                    projects = repository.findAllOrderByCreated(userId);
                else if (comparator == NameComparator.INSTANCE)
                    projects = repository.findAllOrderByName(userId);
                else if (comparator == StatusComparator.INSTANCE)
                    projects = repository.findAllOrderByStatus(userId);
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return projects;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<ProjectDTO> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @NotNull
    @Override
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> projects) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.clear();
                for (@NotNull final ProjectDTO project : projects)
                    repository.add(project);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return projects;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @Override
    public void update(@NotNull final ProjectDTO project) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSQLSession()) {
            try {
                @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
                repository.update(project);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

}