package t1.dkhrunina.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.endpoint.IUserEndpoint;
import t1.dkhrunina.tm.api.service.IServiceLocator;
import t1.dkhrunina.tm.api.service.IUserService;
import t1.dkhrunina.tm.dto.request.user.*;
import t1.dkhrunina.tm.dto.response.user.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.dto.model.SessionDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "t1.dkhrunina.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final UserDTO user;
        try {
            user = getUserService().setPassword(userId, password);
        } catch (@NotNull final Exception e) {
            return new UserChangePasswordResponse(e);
        }
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        try {
            getUserService().lockUserByLogin(login);
        } catch (@NotNull final Exception e) {
            return new UserLockResponse(e);
        }
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegisterResponse registerUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegisterRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable final Role role = request.getRole();
        @Nullable final UserDTO user;
        try {
            user = getUserService().create(login, password, email, role);
        } catch (@NotNull final Exception e) {
            return new UserRegisterResponse(e);
        }
        return new UserRegisterResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final UserDTO user;
        try {
            user = getUserService().removeByLogin(login);
        } catch (@NotNull final Exception e) {
            return new UserRemoveResponse(e);
        }
        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        try {
            getUserService().unlockUserByLogin(login);
        } catch (@NotNull final Exception e) {
            return new UserUnlockResponse(e);
        }
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final UserDTO user;
        try {
            user = getUserService().updateUser(userId, firstName, lastName, middleName);
        } catch (@NotNull final Exception e) {
            return new UserUpdateProfileResponse(e);
        }
        return new UserUpdateProfileResponse(user);
    }

}
