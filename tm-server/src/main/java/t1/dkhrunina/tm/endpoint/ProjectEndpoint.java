package t1.dkhrunina.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.endpoint.IProjectEndpoint;
import t1.dkhrunina.tm.api.service.IProjectService;
import t1.dkhrunina.tm.api.service.IProjectTaskService;
import t1.dkhrunina.tm.api.service.IServiceLocator;
import t1.dkhrunina.tm.dto.request.project.*;
import t1.dkhrunina.tm.dto.response.project.*;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "t1.dkhrunina.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().changeProjectStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            return new ProjectChangeStatusByIdResponse(e);
        }
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        } catch (@NotNull final Exception e) {
            return new ProjectChangeStatusByIndexResponse(e);
        }
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            getProjectTaskService().removeAllByUserId(userId);
        } catch (@NotNull final Exception e) {
            return new ProjectClearResponse(e);
        }
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            return new ProjectCompleteByIdResponse(e);
        }
        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIndexResponse completeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            return new ProjectCompleteByIndexResponse(e);
        }
        return new ProjectCompleteByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().create(userId, name, description);
        } catch (@NotNull final Exception e) {
            return new ProjectCreateResponse(e);
        }
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectFindOneByIdResponse findOneById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectFindOneByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            return new ProjectFindOneByIdResponse(e);
        }
        return new ProjectFindOneByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectFindOneByIndexResponse findOneByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectFindOneByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().findOneByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            return new ProjectFindOneByIndexResponse(e);
        }
        return new ProjectFindOneByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<ProjectDTO> projects;
        try {
            projects = getProjectService().findAll(userId, sort);
        } catch (@NotNull final Exception e) {
            return new ProjectListResponse(e);
        }
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().removeById(userId, id);
        } catch (@NotNull final Exception e) {
            return new ProjectRemoveByIdResponse(e);
        }
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().removeByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            return new ProjectRemoveByIndexResponse(e);
        }
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            return new ProjectStartByIdResponse(e);
        }
        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIndexResponse startProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            return new ProjectStartByIndexResponse(e);
        }
        return new ProjectStartByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().updateById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            return new ProjectUpdateByIdResponse(e);
        }
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project;
        try {
            project = getProjectService().updateByIndex(userId, index, name, description);
        } catch (@NotNull final Exception e) {
            return new ProjectUpdateByIndexResponse(e);
        }
        return new ProjectUpdateByIndexResponse(project);
    }

}